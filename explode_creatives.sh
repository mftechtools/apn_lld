#!/bin/sh
DATE=$1
HOUR=$2
LOGDIR="/var/log/apn_lld/logs"
DATEDIR="$LOGDIR/$1"
REPDIR="$DATEDIR/report"
HOURDIR="$REPDIR/$2"
CHUNKDIR="$REPDIR/chunks"
PANTRY="/pantry/apn/lld/standard/$1/0x00"
RUN="$REPDIR/creatives-fetch.sh"
IDFILE="$PANTRY/$1.creatives"

sort -u $IDFILE > $IDFILE.tmp
mv $IDFILE.tmp $IDFILE
split -l99 $IDFILE $REPDIR/chunks/chunk_
cd $REPDIR
rm last-creative.json
#echo "curl -s -b apn_auth.cookie -c apn_auth.cookie -X POST -d'{\"auth\": {\"username\":\"mfengineer\",\"password\":\"MF4ppn3xusl0g1n!\"}}' \"https://api.appnexus.com/auth\"" >> "$REPDIR/creatives-fetch.sh"
rm "$RUN"
echo "#!/bin/sh" >> "$RUN"
echo "INTERVAL=3" >> "$RUN"
echo "echo \"[\" > creatives.json" >> "$RUN"
echo "curl -s -b apn_auth.cookie -c apn_auth.cookie -X POST -d'{\"auth\": {\"username\":\"mfengineer\",\"password\":\"MF4ppn3xusl0g1n!\"}}' \"https://api.appnexus.com/auth\"" >> "$RUN"
CNT=0
for chunk in $CHUNKDIR/*
do
  NCNT=$(($CNT + 1))
  sed 's/"//g' "$chunk" > $chunk.tmp
  tr "\n" "," < "$chunk.tmp" > $chunk
  rm -f $chunk.tmp
  
  truncate -s -1 "$chunk"
  TIME=`date +%s`
  R=`od -An -N2 -i /dev/random | awk '{print $1}'`
  KEY="$TIME$R"
  awk '{
    print "curl -s -o \"last-creative.json\" -b \"apn_auth.cookie\" -c \"apn_auth.cookie\" \"https://api.appnexus.com/creative?id="$1"\""
  }' "$chunk" > "$REPDIR/creatives-fetch-$TIME$R.sh"
  echo "chmod 700 creatives-fetch-$TIME$R.sh" >> "$RUN"
  echo "./creatives-fetch-$TIME$R.sh" >> "$RUN"
  echo "cat last-creative.json >> creatives.json" >> "$RUN"
  echo "echo \",\" >> creatives.json" >> "$RUN"
  MOD=$(( $NCNT % 10 ))
  if [ $MOD -eq 0 ]; then 
echo "$MOD - $CNT"
     echo "sleep \"\$INTERVAL\"" >> "$RUN"
  fi
  CNT=$NCNT
done
echo "echo \"NULL]\" >> creatives.json" >> "$RUN"
echo "mv creatives.json $PANTRY" >> "$RUN"
chmod 700 "$RUN"
