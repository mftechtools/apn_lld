<?php
 /* APN Log Level Feed Service lab
 *
 * pokes the log level service with a stick and reports on the results:
 *  $> php test_loglevel.php
 *
 * wiki credentials:
 *  uid: mobilefusellc
 *  pwd: appnexus123
 *
 * @author jj
 *
 */
global $path, $open_threads, $thread_limit;
date_default_timezone_set('America/New_York');
$params = [
    'feed' => 'standard_feed',
    'start_date' => null,
    'end_date' => null,
    'start_hour' => null,
];
$params['start_date'] = $argv[1] ?: date('Y-m-d', strtotime('-1 days'));
$params['end_date'] = $argv[2] ?: date('Y-m-d');
$params['start_hour'] = sprintf("%02d", $argv[3]) ?: 0;
$params['test'] = $argv[9] ?: 'no';
$import_dates = getDateRange($params['start_date'], $params['end_date']);

/**
 *
 * https://wiki.appnexus.com/display/api/Authentication+Service
 *
 */
function authenticate() {
    $write_cookie = "apn_auth.cookie";
    $method = "POST";
    $auth_json = json_encode([
        'auth' => [
            'username' => 'mfengineer',
            'password' => 'MF4ppn3xusl0g1n!'
        ]
    ]);
    $endpoint = "https://api.appnexus.com";
    $service = "auth";
    $result = shell_exec("curl -s -b {$write_cookie} -c {$write_cookie} -X {$method} -d'" . $auth_json . "' \"{$endpoint}/{$service}\"");
}

// login and set a cookie, or reuse the cookie if within the ttl
authenticate();
echo "authenticating...\n";
/**
*
*  https://wiki.appnexus.com/display/api/Log-Level+Data+Service
*
*/
// track our experiment
$path = [];

// get the list of available hourly feeds and their splits
if($params['feed'] === 'all') {
    $result = shell_exec("curl -s -b apn_auth.cookie -c apn_auth.cookie 'https://api.appnexus.com/siphon'");
    $feed_names = [];
    foreach($result['response']['siphons'] as $feed) {
        $name = $feed['name'];
        if(!isset($feed_names[$feed['name']])) {
            $feed_names[$feed['name']] = [];
        }
        $feed_names[$feed['name']][] = $feed;
    }
    $path['list_of_feeds'] = [
        'wiki_source' => 'https://wiki.appnexus.com/display/api/Log-Level+Data+Service',
        'result' => $feed_names
    ];
} else {
    foreach($import_dates as $date_stamp) {
        $hour_day = date('Y_m_d_H',$date_stamp);
        $hour = date('H',$date_stamp);
        if(!empty($params['start_hour']) && $params['start_hour'] !== $hour) {
          echo "skipping hour {$hour}\n";
          continue;
        }
        echo "Next Hour: {$date_stamp}  {$hour_day}\n";
        $result = json_decode(shell_exec("curl -s -b apn_auth.cookie -c apn_auth.cookie 'https://api.appnexus.com/siphon?siphon_name={$params['feed']}&hour={$hour_day}'"),true);
        $name = $params['feed'];
        $path['list_of_feeds'] = [
            'wiki_source' => 'https://wiki.appnexus.com/display/api/Log-Level+Data+Service',
            'result' => [$name]
        ];
        if(!isset($feed_names[$name])) {
            $feed_names[$name] = [];
        }

        $feed_names[$name][] = $result;
    }
}
mkdir('/tmp/apn_log_level');

echo "connection...\n";

// log the result to the lab
// grab the first feeds first split
if($params['test'] === 'yes') {
    $some_feed = array_shift($path['list_of_feeds']['result']['response']['siphons']);
    processFeed($some_feed);
} else {
    $feed_name = $path['list_of_feeds']['result'][0];
    $reports = $feed_names[$feed_name];
    foreach($reports as $some_feed) {
        echo "Next Part: {$some_feed['hour']}\n";
        processPart($some_feed['response']['siphons'][0]);
    }
}

/**
$some_feed should look like this:
    array(4) {
      ["name"]=>
      string(18) "bid_landscape_feed"
      ["hour"]=>
      string(13) "2016_08_26_16"
      ["timestamp"]=>
      string(14) "20160826172628"
      ["splits"]=>
      array(1) {
        [0]=>
        array(3) {
          ["part"]=>
          string(1) "0"
          ["status"]=>
          string(3) "new"
          ["checksum"]=>
          string(32) "66737dfc45ebe8e96419d159a5171a0c"
                  }
      }
    }
*/

function processSplit($feed_part_output_file, $feed_hour, $feed_name, $feed_timestamp, $mf_memberid, $feed_split) {
    global $path, $thread_count, $thread_limit;
    // use -L and -o to take the 302 for the file download and pass the output to a local file
    $result = shell_exec("curl -s -L -o \"{$feed_part_output_file}\" -b apn_auth.cookie -c apn_auth.cookie \"https://api.appnexus.com/siphon-download?siphon_name={$feed_name}&hour={$feed_hour}&timestamp={$feed_timestamp}&member_id={$mf_memberid}&split_part={$feed_split}\"");
    $path['get_feed_splits'] = [
        'wiki_source' => 'https://wiki.appnexus.com/display/api/Log-Level+Data+Service',
        'interface' => "curl -s -L -o \"{$feed_part_output_file}\" -b apn_auth.cookie -c apn_auth.cookie \"https://api.appnexus.com/siphon-download?siphon_name={$feed_name}&hour={$feed_hour}&timestamp={$feed_timestamp}&member_id={$mf_memberid}&split_part={$feed_split}\""
        ];

    // reach a conclusion
    if(file_exists($feed_part_output_file)) {
        echo "Success: \n";
        echo "\t{$feed_part_output_file}\n";
    } else {
        echo "The path did not succeed:\n";
    }
}

function processPart($some_feed) {
    global $path;
    
    // set our vars for the next call
    $feed_name = $some_feed['name'];
    $feed_hour = $some_feed['hour'];
    $feed_timestamp = $some_feed['timestamp'];
    $mf_memberid = 2764;

    foreach($some_feed['splits'] as $split) {
        $feed_split = $split['part'];
        $part_checksum = $split['checksum'];
        $feed_part_output_file = "/tmp/apn_log_level/{$part_checksum}-{$feed_name}_{$feed_hour}.{$feed_timestamp}.{$feed_split}.gz";
        processSplit($feed_part_output_file,$feed_hour, $feed_name, $feed_timestamp, $mf_memberid, $feed_split);
    }
}
function getDateRange($start_date, $end_date) {
    $dates = [];

    $low_date_timestamp = $date_timestamp = strtotime($start_date);
    $high_date_timestamp = strtotime($end_date);

    while ($date_timestamp < $high_date_timestamp) {
        $dates[] = $date_timestamp;
        $date_timestamp = strtotime('+1 hour', $date_timestamp);
    }

    return $dates;
}
exit(0);
?>
