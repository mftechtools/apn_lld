#/bin/sh

ALPHA="i-00e3f7c17a689bfd2"
BETA="i-06c3c561eab2b7a99"
GAMMA="i-0700c6ad483196efe"
DELTA="i-0ab0c5e97ad950def"
IIDS="$ALPHA $BETA $GAMMA $DELTA"
aws configure set region us-west-2
aws ec2 start-instances --instance-ids $IIDS
aws ec2 describe-instances --instance-ids $IIDS > lastLaunchDetails.aws
aws ec2 describe-instance-status --instance-ids $IIDS > lastLaunchStatus.aws
