<?php
$date_key = $argv[1];
$part1 = $argv[2];
$part2 = $argv[3];
$part3 = null;
if(!empty($argv[4])) {
  $part3 = $argv[4];
}
$parts = [$part3,$part2,$part1];
$potatoes = [];
while($part=array_pop($parts)) {
  $file = "/pantry/apn/data/{$date_key}_operating-system-extended-{$part}.txt";
  $content = file_get_contents($file);
  $obl = json_decode($content,true);
  $meat = $obl["response"]["operating-systems-extended"];
  $chewed_meat = [];
  while($chewed_meat = array_pop($meat)) {
    $bite = ["id"=>$chewed_meat['id'],"name"=>$chewed_meat['name']];
    $potatoes[] = $bite;
  }
}
$json = json_encode($potatoes);
file_put_contents("/pantry/apn/data/{$date_key}-operating-system-extended-combined.json", $json);
?>
