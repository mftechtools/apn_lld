#!/bin/sh
DATE=$1
HOUR=$2
PART="$1-$2"
echo "digesting ..... $1"
TMPDIR="/tmp/$1"
REPDIR="/var/log/apn_lld/logs/$DATE/report"
HOURDIR="$REPDIR/$HOUR"
PANTRY="/pantry/apn/lld/standard/$DATE/0x00"
NEWNAME="$PANTRY/$PART.parsed"

echo "creating ..... $NEWNAME in $HOURDIR"

gzip -d $HOURDIR/*.gz
mkdir /pantry/apn/lld/standard/$DATE
mkdir $PANTRY
CHUNKDIR="$REPDIR/chunks"
if [ ! -d "$CHUNKDIR" ]; then
	mkdir $CHUNKDIR
fi

awk -F $'\t' '{ 
  if($52 != "NULL" && $52 != 0) print "\""$52"\",\""$2"\",\""$3"\",\""$84"\",\""$85"\",\""$82"\",\""$83"\""
}' $HOURDIR/* > $NEWNAME

#echo "creatinge id file..."

IDFILE="$PANTRY/$1.creatives"
awk -F "," '{ 
  print $1
}' $NEWNAME >> $IDFILE
#sort -u "$IDFILE" > "$IDFILE.tmp"
#mv "$IDFILE.tmp" "$IDFILE"

DEVFILE="$PANTRY/$1.devices"
awk -F $"," '{ 
  print $5
}' $NEWNAME >> $DEVFILE
#sort -u $DEVFILE > $DEVFILE.tmp
#mv $DEVFILE.tmp $DEVFILE

rm -rf $HOURDIR


