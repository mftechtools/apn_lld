<?php
define('TEMPLATES', 'views/');
date_default_timezone_set('America/New_York');
$params = [
    'start_date' => null,
    'end_date' => null
];
$params['start_date'] = $argv[1] ?: date('Y-m-d', strtotime('-1 days'));
$params['end_date'] = $argv[2] ?: date('Y-m-d', strtotime('-1 days'));

$template = TEMPLATES . "_generate.php";
$output = include_once($template);
//echo $output;
file_put_contents("generate.sh", $output);
exit(0);
?>
