<?php
$os_cmd = "php";
$builder = "lld_assembler.php";
$script = "#!/bin/sh\n";
for($i = 24; $i--;) {
  $hour = sprintf("%02d",$i);
  $line = "{$os_cmd} {$builder} {$params['start_date']} {$params['end_date']} {$hour} > hour{$hour}.sh && chmod 700 hour{$hour}.sh";
  $line .= $i > 0 ? " &&": "";
  $script .=  $line . "\n";
}
return $script;
?>
