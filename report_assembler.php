<?php
//error_reporting(0);
date_default_timezone_set('America/New_York');

define('LOGS', "/var/log/apn_lld/logs/");

$params = [
    'start_date' => null,
    'end_date' => null,
    'start_hour' => null
];
$params['start_date'] = $argv[1] ?: date('Y-m-d', strtotime('-1 days'));
$params['end_date'] = $argv[2] ?: date('Y-m-d', strtotime('-1 days'));
//$params['start_hour'] = $argv[3] ? sprintf("%02d", $argv[3]): 0;
$import_dates = getDateRange($params['start_date'], $params['end_date']);

$log_target = LOGS . $params['start_date'];
initializeLog($log_target);
downloadLog($params['start_date'], $log_target);

for($i = 24; $i--;) {
  $hour = sprintf("%02d",$i);
  $hour_dir = $log_target . "/report/{$hour}";
  if(!is_dir($hour_dir)) {
    mkdir($hour_dir);
  }
  $glob = "{$params['start_date']}-{$hour}*";
  $glob_path = "{$log_target}/{$glob}";
  shell_exec("mv {$glob_path} {$hour_dir}"); 
}

exit(0);
function downloadLog($s3Path = null, $dir = null) {
  $cmd = "aws s3 cp --quiet s3://vendor-receiving/lotadata/apn-raw-logs/{$s3Path} {$dir} --recursive";
  //echo $cmd;
  $result = shell_exec("{$cmd}");

}

function initializeLog($dir = null) {
  if(!is_dir($dir)) {
    mkdir($dir);
  }
  if(!is_dir($dir . "/report")) {
    mkdir($dir . "/report");
  }
}

function makeRun($input = null) {


}

function moveParts($dir = null) {

}

function checkDir($dir = null) {

}


//check if the needed logs directory exists
// if it doesnt create it
// download the needed logs from s3 to the logs area
// create a directory for the hours individually
// move all of an hour's parts into its directory
// generate a run file for each part



function getDateRange($start_date, $end_date) {
    $dates = [];

    $low_date_timestamp = $date_timestamp = strtotime($start_date);
    $high_date_timestamp = strtotime($end_date);

    while ($date_timestamp < $high_date_timestamp) {
        $dates[] = $date_timestamp;
        $date_timestamp = strtotime('+1 hour', $date_timestamp);
    }

    return $dates;
}

?>
