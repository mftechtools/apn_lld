#!/bin/sh
SECT=$1
PART=$2
PANTRY="/pantry/apn/lld/standard/$SECT/0x00"
#cd $PANTRY
echo "#!/bin/sh" >> "begin.sh"
for f in $1*.parsed; do
echo "
SECT=\$1
awk -F\",\" '{
  cmd=\"grep \"\$1\" creatives.map\";cmd | getline RR;close(cmd);
  cmd2=\"grep \"\$5\" devices.map\";cmd2 | getline DR;close(cmd2);
   print RR\",\"\$2\",\"\$3\",\"\$4\",\"DR\",\"\$6\",\"\$7
}' $f  > \"$f.csv\"
" >> "$f.run"
echo "./push_reports.sh \$SECT $f.csv" >> "$f.run"
echo "./$f.run $SECT &" >> "begin.sh"
done
