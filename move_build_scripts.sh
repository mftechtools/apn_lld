#!/bin/sh
mkdir rebuild/$1
mv hour* rebuild/$1
cp run.sh rebuild/$1
cp reprocess.sh rebuild/$1
aws s3 mv rebuild/$1 s3://product-sandbox/apn-lld/rebuild-days/$1 --recursive
