#!/bin/sh
START=$1
END=$2
YEST=`date -d "1 days ago" +"%Y-%m-%d"`
TOD=`date +"%Y-%m-%d"`
if [  -z "$START" -a "$START" != " " ]; then
        START="$YEST"
fi
if [  -z "$END" -a "$END" != " " ]; then
        END="$TOD"
fi
cd /usr/local/sbin/apn_loglevel/
php generate.php $START $END
./generate.sh
./run.sh
./move_to_reprocess.sh $START
./move_last_batch.sh $START
./move_to_s3.sh $START
./move_build_scripts.sh $START
