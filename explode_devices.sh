#!/bin/sh
DATE=$1
HOUR=$2
LOGDIR="/var/log/apn_lld/logs"
DATEDIR="$LOGDIR/$1"
PANTRY="/pantry/apn/lld/standard/$1/0x00"
REPDIR="$DATEDIR/report"
HOURDIR="$REPDIR/$2"
IDFILE="$PANTRY/$1.devices"
CHUNKDIR="$REPDIR/chunks"
RUN="$REPDIR/devices-fetch.sh"

sort -u $IDFILE > $IDFILE.tmp
mv $IDFILE.tmp $IDFILE
split -l99 $IDFILE $REPDIR/chunks/devices_
cd $REPDIR
rm last-device.json
#echo "curl -s -b apn_auth.cookie -c apn_auth.cookie -X POST -d'{\"auth\": {\"username\":\"mfengineer\",\"password\":\"MF4ppn3xusl0g1n!\"}}' \"https://api.appnexus.com/auth\"" >> "$REPDIR/creatives-fetch.sh"
rm "$RUN"
echo "#!/bin/sh" >> "$RUN"
echo "INTERVAL=3" >> "$RUN"
echo "echo \"[\" > devices.json" >> "$RUN"
echo "curl -s -b apn_auth.cookie -c apn_auth.cookie -X POST -d'{\"auth\": {\"username\":\"mfengineer\",\"password\":\"MF4ppn3xusl0g1n!\"}}' \"https://api.appnexus.com/auth\"" >> "$RUN"
CNT=0
for chunk in $CHUNKDIR/*
do
  NCNT=$(($CNT + 1))
  sed 's/"//g' "$chunk" > $chunk.tmp
  tr "\n" "," < "$chunk.tmp" > $chunk
  rm -f $chunk.tmp
  
  truncate -s -1 "$chunk"
  TIME=`date +%s`
  R=`od -An -N2 -i /dev/random | awk '{print $1}'`
  KEY="$TIME$R"
  awk '{
    print "curl -s -o \"last-device.json\" -b \"apn_auth.cookie\" -c \"apn_auth.cookie\" \"https://api.appnexus.com/device-model?id="$1"\""
  }' "$chunk" > "$REPDIR/devices-fetch-$TIME$R.sh"
  echo "chmod 700 devices-fetch-$TIME$R.sh" >> "$RUN"
  echo "./devices-fetch-$TIME$R.sh" >> "$RUN"
  echo "cat last-device.json >> devices.json" >> "$RUN"
  echo "echo \",\" >> devices.json" >> "$RUN"
  MOD=$(( $NCNT % 10 ))
  if [ $MOD -eq 0 ]; then 
echo "$MOD - $CNT"
     echo "sleep \"\$INTERVAL\"" >> "$RUN"
  fi
  CNT=$NCNT
done
echo "echo \"NULL]\" >> devices.json" >> "$RUN"
echo "mv devices.json $PANTRY" >> "$RUN"
chmod 700 "$RUN"
