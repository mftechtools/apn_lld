<?php 

$params = [
    'start_date' => null,
];
$params['start_date'] = $argv[1] ?: date('Y-m-d', strtotime('-1 days'));
//$params['start_hour'] = $argv[3] ? sprintf("%02d", $argv[3]): 0;
$pantry = "/pantry/apn/lld/standard/{$params['start_date']}/0x00";

$in = file_get_contents("{$pantry}/creatives.json");
$cr = json_decode($in, true);
$next = [];
$creatives = [];
while($next = array_shift($cr)) {
  if(is_null($next) || empty($next['response']['creatives']) || count($next['response']['creatives']) === 0) { continue; }
  foreach($next['response']['creatives'] as $creative) {
    $creatives[] = $creative;
  }
}
//var_dump(count($creatives));

// need to write id and machine to the map file for grepping
foreach($creatives as $creative) {
  $row = "\"{$creative['id']}\",\"{$creative['name']}\"\n";
  file_put_contents("{$pantry}/creatives.map",$row,FILE_APPEND);
}

?>
