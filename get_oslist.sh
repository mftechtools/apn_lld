#!/bin/sh
TODAY=`date +"%Y-%m-%d"`
php authenticate.php
curl -qs -b apn_auth.cookie -c apn_auth.cookie "https://api.appnexus.com/operating-system-extended?start_element=0&num_elements=100" -o "/pantry/apn/data/"$TODAY"_operating-system-extended-001.txt"
curl -qs -b apn_auth.cookie -c apn_auth.cookie "https://api.appnexus.com/operating-system-extended?start_element=100&num_elements=100" -o "/pantry/apn/data/"$TODAY"_operating-system-extended-002.txt"
php condense_oslist.php $TODAY 001 002
aws s3 cp "/pantry/apn/data/"$TODAY"-operating-system-extended-combined.json" "s3://vendor-receiving/lotadata/apn-datafiles/supplemental/"$TODAY"-operating-system-extended-combined.json"
