<?php 

$params = [
    'start_date' => null,
];
$params['start_date'] = $argv[1] ?: date('Y-m-d', strtotime('-1 days'));
//$params['start_hour'] = $argv[3] ? sprintf("%02d", $argv[3]): 0;
$pantry = "/pantry/apn/lld/standard/{$params['start_date']}/0x00";

$in = file_get_contents("{$pantry}/devices.json");
$cr = json_decode($in, true);
$next = [];
$devices = [];
while($next = array_shift($cr)) {
  if(empty($next['response']['device-models']) || is_null($next) || count($next['response']['device-models']) === 0) { continue; }
  foreach($next['response']['device-models'] as $device) {
    $devices[] = $device;
  }
}
//var_dump(count($creatives));

// need to write id and machine to the map file for grepping
foreach($devices as $device) {
  $row = "\"{$device['id']}\",\"{$device['name']} {$device['device_make_name']}\"\n";
  file_put_contents("{$pantry}/devices.map",$row,FILE_APPEND);
}

?>
