<?php
define('TEMPLATES', 'views/');
date_default_timezone_set('America/New_York');
$params = [
    'start_date' => null,
    'end_date' => null
];
$params['start_date'] = $argv[1] ?: date('Y-m-d', strtotime('-1 days'));
$params['end_date'] = $argv[2] ?: date('Y-m-d');
$reprocess_dir = "/var/log/apn_lld/reprocess";
$reprocess_files = [];
if ($handle = opendir($reprocess_dir)) {
    //echo "Directory handle: $handle\n";
    //echo "Entries:\n";

    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
      if($entry !== "." && $entry !== "..") {
        $parts = explode('.', $entry);
        $ret = explode('-', $parts[0]);
        $reprocess_files[] = "./hour" . $ret[2] . ".sh";
      }
    }
    closedir($handle);
}
//var_dump($reprocess_files);

$template = TEMPLATES . "_reprocess.php";
$output = include_once($template);
//echo $output;
file_put_contents("reprocess.sh", $output);
exit(0);
?>
